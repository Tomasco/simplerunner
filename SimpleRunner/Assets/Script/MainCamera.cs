﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

    private Vector3 m_RotationCenter;
    private bool m_Orbit;
    private float m_RevolutionTime = 5f;

	void Update () {
		print ("hghh");
        if (m_Orbit)
        {
            float angles = 360f / m_RevolutionTime;
            transform.RotateAround(m_RotationCenter, Vector3.up, angles * Time.deltaTime); 
        }
    }
    public void StartOrbit (Vector3 rotationCenter){
        m_RotationCenter = rotationCenter;
        m_Orbit = true;
    }
}
