﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotCap : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	[SerializeField]//solo aplica para el que esta abajo. serializar es para poder exponer la variable al editor.
	private Transform m_RaceCenter;
	[SerializeField]
	private float m_SecondsByLap;
	[SerializeField]
	private float m_CurrentSpeed = 1f;
	//[SerializeField]
	//public float speed = 10;
	[SerializeField]
	private float m_MaxDistance = 2f;
	[SerializeField]
	private float m_MinDistance = 1f;
	[SerializeField]
	public float m_SideSpeed = 20f;
	private bool m_Running = true;
	[SerializeField]
	private int m_TotalLaps=3;
	private bool isRunning;
	[SerializeField]
	private int m_CurrentLap;
	[SerializeField]
	private float m_MaxSpeed = 3f;
	[SerializeField]
	private float m_SpeedIncrement = 0.1f;
	[SerializeField]
	private float m_MiniSpeed = 1f;
	private float m_SpeedDecrementeByScecond = 0.03f;
	private float m_ElapsedTime = 0f;



	void Update () {
		if (!m_Running) {
			return;

		}
		m_ElapsedTime += Time.deltaTime;
		if (m_CurrentSpeed > m_MiniSpeed) {
			m_CurrentSpeed -= m_SpeedDecrementeByScecond * Time.deltaTime;
		}


		float angles = 360f / m_SecondsByLap;

		transform.RotateAround (m_RaceCenter.position, Vector3.up, angles * Time.deltaTime * m_CurrentSpeed);
	//	if (Input.GetAxis ("Horizontal") > 0)			
	//	transform.Translate (new Vector3 (0, 0, speed) * Time.deltaTime);//transform = componente de los objetos . translate =position
	//	else if (Input.GetAxis ("Horizontal") < 0)
	//		transform.Translate (new Vector3 (0, 0, -speed) * Time.deltaTime);

		float inputHorizontal = Input.GetAxis("Horizontal");					
		float xtranslation = inputHorizontal * Time.deltaTime * m_SideSpeed;	

		float distance = Vector3.Distance (m_RaceCenter.position, transform.position);
		if (inputHorizontal > 0f && distance > m_MinDistance || inputHorizontal < 0f && distance < m_MaxDistance){
			transform.Translate(new Vector3(xtranslation,0f, 0f));	
		}
	}
	private void EndRace(){
		Camera.main.gameObject.AddComponent<MainCamera> ().StartOrbit (transform.position);
		isRunning = false;
		m_Running = false;
		Debug.Log ("Total time: " + m_ElapsedTime.ToString ());

	}
	private void OnTriggerEnter (Collider collider)
	{
		if (collider.gameObject.tag == "Tag0") {//tag= en los objetos tienen una "tag" para llamarlo 
			m_CurrentLap++;

			if (m_CurrentLap >= m_TotalLaps) {
				EndRace ();

			} 
		
		}
		else if (collider.gameObject.tag == "PickUp") {
			
			if (m_CurrentSpeed < m_MaxSpeed) {
				Debug.Log ("la vel es menor: " + m_CurrentSpeed.ToString ());
				m_CurrentSpeed += m_SpeedIncrement;
				Destroy (collider.gameObject);
			}
		}
	}
}
